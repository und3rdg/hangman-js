var quotList = [
   "Who looks outside, dreams. Who looks inside, awakes."
  ,"stupid is as stupid does."
  ,"be better than you were yesterday."
  ,"Actions speak louder than words."
  ,"Truth is stranger than fiction."
  ,"fiction has to make sense."
  ,"Where there is a will there`s a way."
  ,"No man has a good enough memory to be a successful liar."
  ,"You cannot escape the responsibility of tomorrow by evading it today."
  ,"What you do today can improve all your tomorrows."
  ,"The power of imagination makes us infinite."
  ,"In teaching others we teach ourselves."
  ,"Life is trying things to see if they work."
  ,"It does not matter how slowly you go as long as you do not stop."
  ,"Everything you can imagine is real."
  ,"Whatever you are be a good one."
  ,"May you live every day of your life."
  ,"If you have never failed you have never lived."
  ,"It is never too late to be what you might have been."
  ,"We become what we think about."
  ,"All you need is love."
  ,"Hope is a waking dream."
  ,"An obstacle is often a stepping stone."
  ,"The power of imagination makes us infinite."
  ,"In teaching others we teach ourselves."
  ,"Don’t regret the past, just learn from it."
  ,"Life is trying things to see if they work."
  ,"It does not matter how slowly you go as long as you do not stop."
  ,"I hear and I forget, I see and I remember. I do and I understand."
  ,"Wherever you go, go with all your heart."
  ,"Everything you can imagine is real."
  ,"Don't wait. The time will never be just right."
  ,"Men are born to succeed, not fail."
  ,"Whatever you are, be a good one."
  ,"May you live every day of your life."
  ,"If you have never failed you have never lived."
  ,"Ever tried. Ever failed. No matter. Try Again. Fail again. Fail better."
  ,"It is never too late to be what you might have been."
  ,"Do what you can, with what you have, where you are."
  ,"We become what we think about."
  ,"All you need is love."
  ,"Hope is a waking dream."
  ,"An obstacle is often a stepping stone."
  ,"Action is the foundational key to all success."
  ,"Be not afraid of going slowly, be afraid only of standing still."
  ,"Eighty percent of success is showing up."
  ,"Never, never, never give up."
  ,"The best revenge is massive success."
  ,"Learn from yesterday, live for today, hope for tomorrow."
  ,"All our words from loose using have lost their edge."
  ,"If you dream it, you can do it."
  ,"Turn your wounds into wisdom."
  ,"Believe you can and you’re halfway there."
  ,"Live what you love."
  ,"The best way out is always through."
  ,"A jug fills drop by drop."
  ,"Dream big and dare to fail."
  ,"Do one thing every day that scares you."
  ,"Hope is the heartbeat of the soul."
  ,"God couldnt"
  ,"You must do the thing you think you cannot do."
  ,"The obstacle is the path."
  ,"If not us, who? If not now, when?"
  ,"To be the best, you must be able to handle the worst."
  ,"I can, therefore I am."
  // ,"1"
];


// FILTERING LIST TO QUOTES NOT LONGER THAN
var maxLengthQuotes = 35
var quotListFiltered = [];
for ( var i=0; i<quotList.length; i++)
{
    if (quotList[i].length < maxLengthQuotes)
    {
        quotListFiltered[quotListFiltered.length] = quotList[i];
        // quotListFiltered.push(quotList[i]);
        // quotListFiltered = quotListFiltered.concat(quotList[i]);
       // document.write(quotListFiltered + '<br />');
    }
}

// PICK ONE RANDOM QUOTE FROM FILTERED LIST
var quotQuant = quotListFiltered.length;
var quotNum = Math.floor(Math.random() * quotQuant);


// FIXED QUOTE FOR TESTS
// var quotNum = quotListFiltered.length; 
// quotListFiltered.push("aaaaaaaa!!!");

var quot = quotListFiltered[quotNum];
quot = quot.toUpperCase();


var quotHide = "";
for (i=0; i<quot.length; i++)
{
    if (quot.charAt(i)==" ") quotHide = quotHide + " ";
    else if (quot.charAt(i)==".") quotHide = quotHide + ".";
    else if (quot.charAt(i)==",") quotHide = quotHide + ",";
    else if (quot.charAt(i)=="'") quotHide = quotHide + "'";
    else if (quot.charAt(i)=="?") quotHide = quotHide + "?";
    else if (quot.charAt(i)=="!") quotHide = quotHide + "!";
    else quotHide = quotHide + "-";
}


function quotHideShow()
{
    document.getElementById("plansza").innerHTML = quotHide;
}

window.onload = start;

var leters = new Array(26);
leters[0] = "A";
leters[1] = "B";
leters[2] = "C";
leters[3] = "D";
leters[4] = "E";
leters[5] = "F";
leters[6] = "G";
leters[7] = "H";
leters[8] = "I";
leters[9] = "J";
leters[10] = "K";
leters[11] = "L";
leters[12] = "M";
leters[13] = "N";
leters[14] = "O";
leters[15] = "P";
leters[16] = "Q";
leters[17] = "R";
leters[18] = "S";
leters[19] = "T";
leters[20] = "U";
leters[21] = "V";
leters[22] = "W";
leters[23] = "X";
leters[24] = "Y";
leters[25] = "Z";

function start()
{
    var divLetersContent = "";

    for (i=0; i<=25;i++)
    {
        var element = "lit" + i;
        divLetersContent = divLetersContent + '<div class="litera" onclick="check('+i+')" id="'+element+'">'+leters[i]+'</div>';
        if ( (i+1) % 6 == 0) divLetersContent = divLetersContent + '<div style="clear:both;"></div>';
    }
    document.getElementById("alfabet").innerHTML = divLetersContent;

    quotHideShow();
}

String.prototype.setLeter = function(place, leter)
{
    if (place > this.length -1)
    {
        return this.toString();
    } else return this.substr(0, place) + leter + this.substr(place+1);
}


// CHECKING ANSWERS
function check(nr)
{
    var yes =   new Audio("js/szubienica/yes.wav");
    var no =    new Audio("js/szubienica/no.wav");
    var win =   new Audio("js/szubienica/win.wav");
    var loose = new Audio("js/szubienica/loose.wav");

    var failLetterCount = 0;
    var correctLeter = false;

    // IF LETER IS IN QUOTE SHOW IT
    for(i=0; i<quot.length; i++)
    {
        if (quot.charAt(i) == leters[nr])
        {
            quotHide = quotHide.setLeter(i,leters[nr]);
            correctLeter = true;
        }
    }
    if (correctLeter == true)
    {
        //AND PLAY SOUND, MARK IT GREEN
        yes.play();
        var element = "lit" + nr;
        document.getElementById(element).style.background = "#030";
        document.getElementById(element).style.color = "#00c000";
        document.getElementById(element).style.border = "3px solid #00c000";
        document.getElementById(element).style.cursor = "default";
        quotHideShow();
    } else
    {
        // IF not...
        no.play();
        var element = "lit" + nr;
        document.getElementById(element).style.background = "#330000";
        document.getElementById(element).style.color = "#c00000";
        document.getElementById(element).style.border = "3px solid #c00000";
        document.getElementById(element).style.cursor = "default";
        document.getElementById(element).setAttribute("onclick",";");

        //... and increase fail counter
        failLetterCount++;
        var hangManPic = "img/s" + failLetterCount + ".jpg";
        document.getElementById("szubienica").innerHTML = '<img src="'+hangManPic+'" alt="" />'
    }

    // WIN
    if (quot == quotHide)
    {
        win.play();
        document.getElementById("alfabet").innerHTML = 'Good! Corect answer:</br /><span class="quot">'+quot+'</span><br /></br /><span class="reset" onclick="location.reload()">AGAIN?</span>';
        // FIREWORKS
        myFW1.start();
        myFW2.start();
        myFW3.start();
        myFW4.start();
        return false;
    }

    // LOOSE
    if (failLetterCount>=9)
    {
        loose.play();
        document.getElementById("alfabet").innerHTML = 'Loose! Correct answer:</br /><span class="quot">'+quot+'</span><br /></br /><span class="reset" onclick="location.reload()">AGAIN?</span>';
    }
}
